import Route from '@ioc:Adonis/Core/Route'

Route.group(() => {
  // Rotas para atletas
  Route.resource('athletes', 'AthleteController').apiOnly()

  // Rotas para treinos
  Route.resource('trainings', 'TrainingController').apiOnly()

  // Outras rotas, como autenticação, etc.
}).prefix('api/v1')
