import { schema, rules } from '@ioc:Adonis/Core/Validator'

export default class AthleteValidator {
  public schema = schema.create({
    name: schema.string({ trim: true }),
    email: schema.string({ trim: true }, [
      rules.email(),
      rules.unique({ table: 'athletes', column: 'email' }),
    ]),
  })

  public messages = {
    'name.required': 'Please provide a name for the athlete',
    'email.required': 'Please provide an email address for the athlete',
    'email.email': 'Please provide a valid email address',
    'email.unique': 'Email address is already in use',
  }
}
