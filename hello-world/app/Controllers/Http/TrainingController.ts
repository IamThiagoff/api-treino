import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Training from 'App/Models/Training'
import TrainingValidator from 'App/Validators/TrainingValidator'

export default class TrainingController {
  public async index({}: HttpContextContract) {
    const trainings = await Training.all()
    return trainings
  }

  public async store({ request, response }: HttpContextContract) {
    try {
      await request.validate(TrainingValidator)

      const data = request.only(['title', 'description', 'athlete_id'])
      const training = await Training.create(data)

      return training
    } catch (error) {
      response.status(400).send(error.messages)
    }
  }

  public async show({ params, response }: HttpContextContract) {
    try {
      const training = await Training.findOrFail(params.id)
      return training
    } catch (error) {
      response.status(404).send({ message: 'Training not found' })
    }
  }

  public async update({ params, request, response }: HttpContextContract) {
    try {
      const training = await Training.findOrFail(params.id)

      await request.validate(TrainingValidator)

      const data = request.only(['title', 'description', 'athlete_id'])
      training.merge(data)
      await training.save()

      return training
    } catch (error) {
      response.status(400).send(error.messages)
    }
  }

  public async destroy({ params, response }: HttpContextContract) {
    try {
      const training = await Training.findOrFail(params.id)
      await training.delete()
      return { message: 'Training deleted successfully' }
    } catch (error) {
      response.status(404).send({ message: 'Training not found' })
    }
  }
}
