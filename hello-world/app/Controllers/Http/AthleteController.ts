import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Athlete from 'App/Models/Athlete'

export default class AthleteController {
  public async index({}: HttpContextContract) {
    const athletes = await Athlete.all()
    return athletes
  }

  public async store({ request }: HttpContextContract) {
    const data = request.only(['name', 'email'])
    const athlete = await Athlete.create(data)
    return athlete
  }

  public async show({ params }: HttpContextContract) {
    const athlete = await Athlete.findOrFail(params.id)
    return athlete
  }

  public async update({ params, request }: HttpContextContract) {
    const athlete = await Athlete.findOrFail(params.id)
    const data = request.only(['name', 'email'])
    athlete.merge(data)
    await athlete.save()
    return athlete
  }

  public async destroy({ params }: HttpContextContract) {
    const athlete = await Athlete.findOrFail(params.id)
    await athlete.delete()
    return { message: 'Athlete deleted successfully' }
  }
}
