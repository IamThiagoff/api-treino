import BaseSchema from '@ioc:Adonis/Lucid/Schema'

export default class CreateTrainings extends BaseSchema {
  protected tableName = 'trainings'

  public async up () {
    this.schema.createTable(this.tableName, (table) => {
      table.increments('id')
      table.string('title').notNullable()
      table.text('description').nullable()
      table.integer('athlete_id').unsigned().references('id').inTable('athletes').onDelete('CASCADE')
      table.timestamps(true, true)
    })
  }

  public async down () {
    this.schema.dropTable(this.tableName)
  }
}
